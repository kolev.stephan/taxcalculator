﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaxCalculator.Core.interfaces
{
    public class ImaginariaIncomeTax : IIncomeTax
    {
        public decimal TaxRate { get; set; }
        public decimal TaxExpense { get; set; }
        public decimal IncomeTaxFreeAmount { get; set; }

        public ImaginariaIncomeTax(decimal tax, decimal freeamount = 1000)
        {
            TaxRate = tax;
            IncomeTaxFreeAmount = freeamount;
        }
    }
}
