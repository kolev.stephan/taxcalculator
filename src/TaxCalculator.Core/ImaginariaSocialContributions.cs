﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaxCalculator.Core
{
    public class ImaginariaSocialContributions : ISocialContributions
    {
        public decimal SocialTaxRate { get ; set; }
        public decimal TaxExpense { get ; set ; }
        public decimal MaxSocialRateApplied { get ; set ; }

        public ImaginariaSocialContributions(decimal taxRate, decimal taxFreeAmount = 3000)
        {
            SocialTaxRate = taxRate;
            MaxSocialRateApplied = taxFreeAmount;
        }
    }
}
