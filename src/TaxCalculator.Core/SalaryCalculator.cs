﻿using System;
using System.Collections.Generic;
using System.Text;
using TaxCalculator.Core.enums;
using TaxCalculator.Core.interfaces;

namespace TaxCalculator.Core
{
    public class SalaryCalculator
    {
        public decimal GrossSalaryAmount { get; private set; }
        
        private SalaryCalculator(decimal grossamount)
        {
            GrossSalaryAmount = grossamount;
        }

        public static SalaryCalculator SetGrossAmount(decimal grossAmount)
        {
            if (grossAmount == default(decimal) || grossAmount <= 0)
                throw new ArgumentException($"Gross salary amount {grossAmount} is not valid.");
            return new SalaryCalculator(grossAmount);
        }
    }
}
