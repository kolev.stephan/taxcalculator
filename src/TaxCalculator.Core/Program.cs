﻿using System;
using TaxCalculator.Core.enums;

namespace TaxCalculator.Core
{
    public class Program
    {
        public static void Main()
        {
            var netAmount = SalaryCalculator.SetGrossAmount(3400)
                .ForCountry(TaxCountry.Imaginaria)
                .ApplyIncomeTax()
                .ApplySocialContributionTax()
                .Calculate();
        }
    }
}
