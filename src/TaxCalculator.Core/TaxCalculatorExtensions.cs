﻿using System;
using TaxCalculator.Core.enums;
using TaxCalculator.Core.interfaces;

namespace TaxCalculator.Core
{
    public static class TaxCalculatorExtensions
    {
        public static ICountySpecificCalculator ForCountry(this SalaryCalculator calculator, TaxCountry country)
        {
            switch (country)
            {
                case TaxCountry.Imaginaria:
                    var incomeTax = new ImaginariaIncomeTax(0.1M);
                    var socials = new ImaginariaSocialContributions(0.15M);
                    return new ImaginariaTaxCalculator(calculator.GrossSalaryAmount, incomeTax, socials);

                default:
                    throw new NotImplementedException($" Country {Enum.GetName(typeof(TaxCountry), country)} not found.");
            }
        }
    }
}
