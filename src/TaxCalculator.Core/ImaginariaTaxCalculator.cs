﻿using System;
using System.Collections.Generic;
using System.Text;
using TaxCalculator.Core.interfaces;

namespace TaxCalculator.Core
{
    public class ImaginariaTaxCalculator : ICountySpecificCalculator
    {
        private readonly decimal _grossSalaryAmount;
        private readonly IIncomeTax _incomeTax;
        private readonly ISocialContributions _socialContributions;

        public decimal TaxableIncome => _grossSalaryAmount - _incomeTax.IncomeTaxFreeAmount; 
        
        public ImaginariaTaxCalculator(decimal grossSalary, IIncomeTax incomeTax, ISocialContributions socials)
        {
            _grossSalaryAmount = grossSalary;
            _incomeTax = incomeTax ?? throw new ArgumentException(nameof(incomeTax));
            _socialContributions = socials ?? throw new ArgumentException(nameof(socials));
        }

        public IIncomeTax IncomeTaxSettings { get; set; }

        public ISocialContributions SocialContributionsSettings { get; set; }

        public ICountySpecificCalculator ApplyIncomeTax()
        {
            if (TaxableIncome > 0)
                _incomeTax.TaxExpense = (_grossSalaryAmount - _incomeTax.IncomeTaxFreeAmount) * _incomeTax.TaxRate;
            return this;
        }

        public ICountySpecificCalculator ApplySocialContributionTax()
        {
            if (TaxableIncome > 0 && TaxableIncome < _socialContributions.MaxSocialRateApplied)
            {
                _socialContributions.TaxExpense = Math.Floor((_grossSalaryAmount - _incomeTax.IncomeTaxFreeAmount) / 1000) * _socialContributions.SocialTaxRate * 1000;
            }
                
            return this;
        }

        public decimal Calculate()
        {
            return _grossSalaryAmount - _incomeTax.TaxExpense - _socialContributions.TaxExpense;
        }
    }
}
