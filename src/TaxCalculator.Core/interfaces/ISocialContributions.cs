﻿namespace TaxCalculator.Core
{
    public interface ISocialContributions
    {
        decimal SocialTaxRate { get; set; }

        decimal TaxExpense { get; set; }

        decimal MaxSocialRateApplied { get; set; }
    }
}