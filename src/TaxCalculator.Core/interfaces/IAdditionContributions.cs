﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaxCalculator.Core.interfaces
{
    public interface IAdditionContributions : IIncomeTax
    {
        decimal AdditionalTaxExpense { get; set; }
    }
}
