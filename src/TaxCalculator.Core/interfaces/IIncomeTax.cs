﻿namespace TaxCalculator.Core
{
    public interface IIncomeTax
    {
        decimal TaxRate { get; set; }
        decimal TaxExpense { get; set; }
        decimal IncomeTaxFreeAmount { get; set; }
    }
}