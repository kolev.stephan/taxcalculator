﻿using System;
using System.Collections.Generic;
using System.Text;
using TaxCalculator.Core.interfaces;

namespace TaxCalculator.Core
{
    public interface ICountySpecificCalculator
    {
        IIncomeTax IncomeTaxSettings { get; set; }
        ISocialContributions SocialContributionsSettings { get; set; }
        ICountySpecificCalculator ApplyIncomeTax();
        ICountySpecificCalculator ApplySocialContributionTax();
        decimal Calculate();

    }
}
